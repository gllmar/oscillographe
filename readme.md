En continuité avec le mouvement de musique pour oscilloscope, Oscillographe suscite une expérience sinestésique où son deviens l'image.

## [Oscillographe](http://gllmar.github.io/projets/oscillo/)

## Dependencies
### Openframeworks (0.10+)
### Addons
```
cd $OF_ROOT/addons
git clone https://github.com/gllmAR/ofxBasicSoundPlayer.git
```

### Dependences

```
https://github.com/yoggy/sendosc
```

```
cd ~/src
sudo apt-get install liboscpack-dev cmake
git clone https://github.com/yoggy/sendosc.git
cd sendosc
cmake .
make
sudo make install  
```

## Déploiement sur Raspberry pi

### Installation Arch linux via linux 

#### Partitions:
```
sudo fdisk /dev/sdX 
```

```
o -> clear 
p -> list partition
n -> new
p -> primary
1 -> first
accept default sector
+100M -> last 
	(remove signature yes)
t -> type
c -> W95 AT32 (LBA)
n -> new
p -> primary
2 -> second
accept default start sector
accept default end sector
w -> write
```

#### create file system
```
sudo mkfs.vfat /dev/sdX1
mkdir boot
sudo mount /dev/sdX1 boot
```

```
sudo mkfs.ext4 /dev/sdX2
mkdir root
sudo mount /dev/sdX2 root

```

#### deploy file system
```
wget http://os.archlinuxarm.org/os/ArchLinuxARM-rpi-2-latest.tar.gz
sudo bsdtar -xpf ArchLinuxARM-rpi-2-latest.tar.gz -C root
sync
sudo mv root/boot/* boot
sudo umount boot root
```
unplug uSd card, boot raspberry pi

### manage userspace 

#### create user / manage key
```
ssh alarm@[ip]
su [root]
pacman-key --init
pacman-key --populate archlinuxarm
passwd
useradd -m -g users -G audio,wheel,storage,power,uucp,lock artificiel
passwd artificiel
pacman -Sy sudo git base-devel
nano /etc/sudoers
	-> comment out wheel (both)
exit
exit
ssh artificiel@[ip]
sudo userdel alarm
```

#### get ssh id (need a artificiel account)
```
mkdir ~/aur && cd ~/aur
git -c http.sslVerify=false clone https://gitlab.artificiel.org:30000/aur/rsa-cloudclient.git 
cd rsa-cloudclient 
./pre-makepkg.sh
makepkg -sri
./install.sh

```
#### get maja (need a artificiel account)
```
cd ~/aur 
git clone ssh://git@gitlab.artificiel.org:30001/aur/maja.git
cd maja
./install.sh
```

#### configure core system via maja (need a artificiel account)
```
maja -i -g ssh://git@gitlab.artificiel.org:30001/aur/core-system.git
```

#### configure hardware 
#### audio
```
sudo pacman -S alsa-utils
```
add to /boot/config.txt

```
dtparam=audio=on
audio_pwm_mode=2
```

video?
```
sudo pacman -S xf86-video-fbdev xf86-video-fbturbo-git
```
### install openframeworks
```
maja -i -g ssh://git@gitlab.artificiel.org:30001/aur/openframeworks-git.git
```

#### get libs for armv7
```
cd $OF_ROOT
cd /scripts/linux
sudo ./download_libs.sh --platform linuxarmv7l
cd /archlinux_armv7
sudo sh install_dependencies.sh
```

#### flags in profile
```
sudo nano /etc/profile.d/of_pi.sh
```
```
export MAKEFLAGS=-j4 PLATFORM_ARCH=armv7l PLATFORM_VARIANT=raspberry2
```
#### Get addons
```
cd $OF_ROOT
cd addons
git clone https://github.com/gllmAR/ofxBasicSoundPlayer

```

#### Clone this repo
```
mkdir ~/src && cd src 
git clone git@gitlab.com:gllmAR/oscillographe.git

```

#### make the projet
```
cd oscillographe
make
```

### Workarounds Party

Audio input on the raspberry pi is still not ready for prime time. Somes optimisations make the experience smoother

#### Test speakers

`aplay -L ` permet de sortir le nom des carte en format speaker test compatible

`speaker-test -c 2 -D sysdefault:CARD=CODEC ` permet de tester carte usb


#### config.txt

activate audio tweak over scan and gpu memory

/boot/config.txt 
```
dtparam=audio=on
audio_pwm_mode=2
gpu_mem=256
disable_overscan=1
disable_audio_dither
```

#### commandline.txt

* drop USB to 1.1 
	* This really incapacitate the standart USB I/O but it is still the only way to get click less audio input.
* tweak som fiq_fsm_mask

/boot/commandline.txt

```
root=/dev/mmcblk0p2 rw rootwait console=ttyAMA0,115200 console=tty1 selinux=0 plymouth.enable=0 smsc95xx.turbo_mode=N dwc_otg.lpm_enable=0 kgdboc=ttyAMA0,115200 elevator=noop dwc_otg.fiq_enable=1 dwc_otg.fiq_fsm_enable=1 dwc_otg.fiq_fsm_mask=0xF dwc_otg.nak_holdoff=1 dwc_otg.fiq_fsm_mask=0x4 dwc_otg.speed=1
```

### configure wireless

https://www.raspberrypi.org/documentation/configuration/wireless/wireless-cli.md

```
sudo nano /etc/systemd/network/50-wlan0_dhcp.network
```
```
[Match]
Name=wlan*

[Network]
DHCP=ipv4
```
```
sudo nano /etc/wpa_supplicant/wpa_supplicant-wlan0.conf
```

```
ctrl_interface=/var/run/wpa_supplicant
ctrl_interface_group=wheel
eapol_version=1
ap_scan=1
fast_reauth=1
update_config=1

network={
    ssid="SSID_NAME_HERE"
    psk="PASS_WORD"
    priority=1
}

network={
	ssid="SSID2_NAME_HERE"
    psk="PASS_WORD"
    priority=1
}



```

```
sudo rm /etc/resolv.conf
sudo  systemctl enable wpa_supplicant@wlan0
sudo systemctl start wpa_supplicant@wlan0
sudo systemctl start systemd-resolved
sudo ln -s /run/systemd/resolve/resolv.conf /etc/resolv.conf
sudo systemctl restart systemd-networkd

```

### open-stage-control

```
maja -i -g https://aur.archlinux.org/open-stage-control.git
```

### Troubleshoot
```
if software doesn't start, try to remove bin/data/setup.json
```
