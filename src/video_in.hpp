//
//  video_in.hpp
//  oscillographe
//
//  Created by Guillaume Arseneault on 2022-10-09.
//
//

#ifndef video_in_hpp
#define video_in_hpp

#include "ofMain.h"
#include "ofxGui.h"
#include "presets.hpp"



class Video_in

{
public:
    string video_in = "video_in";
    ofxGuiGroup gui;
    
    void draw_gui();
    ofVideoGrabber vidGrabber;

    int gui_x_offset=0;
    int gui_y_offset=0;
    
    ofParameter<bool> enable = 1;
    ofParameter<float> scale= 1;
    ofParameter<float> alpha=1;
    ofParameter<int> rotate=0;
   
    
    void setup();

    void update();
    void draw();
    void set_size(int w, int h);
    
    int camWidth;
    int camHeight;
    
    int app_size_w = 100000;
    int app_size_h = 100000;
    
    
    // preset
    Presets presets;

};

#endif /* graphe_hpp */
