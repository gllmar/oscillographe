//
//  video_in_
//  oscillographe
//
//  Created by Guillaume Arseneault on 17-11-09.
//
//

#include "video_in.hpp"

//--------------------------------------------------------------
void Video_in::setup()
{   
    //settings_gui.setup("settings");
    //presets.setup("video_in");
    //presets.recalled_gui.add(enable.set("enable",1));
    gui.setup(video_in);
    gui.add(enable.set("enable",0));
    gui.add(scale.set("scale",1, 0, 2));
    gui.add(alpha.set("alpha",1, 0, 1));
    gui.add(rotate.set("rotate",0, 360, 0));
 if (enable)
    {
    camWidth = 640;  // try to grab at this size.
    camHeight = 360;
    vector<ofVideoDevice> devices = vidGrabber.listDevices();
    for(size_t i = 0; i < devices.size(); i++){
        if(devices[i].bAvailable){
            //log the device
            ofLogNotice() << devices[i].id << ": " << devices[i].deviceName;
        }else{
            //log the device and note it as unavailable
            ofLogNotice() << devices[i].id << ": " << devices[i].deviceName << " - unavailable ";
        }
    }
    vidGrabber.setDeviceID(0);
    vidGrabber.setDesiredFrameRate(60);
    vidGrabber.initGrabber(camWidth, camHeight);
    }
    

}

//--------------------------------------------------------------
void Video_in::update()
{
    if (enable)
    {
    vidGrabber.update();

    }
    
}

//--------------------------------------------------------------
void Video_in::draw()
{
    if (enable)
    {
        // draw the OSCILO channel:
        ofPushStyle();
        ofPushMatrix();
    // drawheres
       ofSetColor(255, 255, 255, alpha*255);
       ofScale(scale*1.0, scale*1.0, 1.0);
       int x=ofGetScreenWidth();
       int y=ofGetScreenHeight();
       ofRotateDeg(rotate,1,0,0);
        vidGrabber.draw(0,0, x,y );
        ofPopMatrix();
        ofPopStyle();
    }
}

//--------------------------------------------------------------


//--------------------------------------------------------------


//--------------------------------------------------------------


//--------------------------------------------------------------


//--------------------------------------------------------------



//--------------------------------------------------------------

//--------------------------------------------------------------



