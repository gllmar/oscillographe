//
//  video_gst_in_
//  oscillographe
//
//  Created by Guillaume Arseneault on 17-11-09.
//
//

#include "video_gst_in.hpp"

//--------------------------------------------------------------
void Video_gst_in::setup()
{   

    gui.setup(video_in);
    gui.add(enable.set("enable",1));
    gui.add(scale.set("scale",1, 0, 2));
    gui.add(alpha.set("alpha",1, 0, 1));
    gui.add(rotate180.set("rotate180",0));
    


    gst.setPipeline("libcamerasrc ! video/x-raw,width=640,height=480,framerate=60/1  ! videoconvert  " , OF_PIXELS_RGB, true, camWidth, camHeight);
    gst.startPipeline();
    gst.play();
    tex.allocate(camWidth,camHeight,GL_RGB);
    

}

//--------------------------------------------------------------
void Video_gst_in::update()
{
    if (enable)
    {
       gst.update();
    if(gst.isFrameNew())
    {
        if (tex.getWidth() != gst.getWidth())
        {
            tex.allocate(gst.getWidth(), gst.getHeight(), GL_RGB);
        }
        tex.loadData(gst.getPixels(), GL_RGB);
    }
    
    }
}    

//--------------------------------------------------------------
void Video_gst_in::draw()
{
    if (enable)
    {
        // draw the OSCILO channel:
        ofPushStyle();
        ofPushMatrix();
    // drawheres
       ofSetColor(255, 255, 255, alpha*255);
       ofScale(scale*1.0, scale*1.0, 1.0);
        int x=ofGetWidth();
       int y=ofGetHeight();
       if (rotate180)
       {
        tex.setAnchorPoint(x,y);
        ofRotate(180.0);
       } else {
        tex.setAnchorPoint(0,0);
       }
        tex.draw(0,0, x, y);
        ofPopMatrix();
        ofPopStyle();
    }
}

//--------------------------------------------------------------


//--------------------------------------------------------------


//--------------------------------------------------------------


//--------------------------------------------------------------


//--------------------------------------------------------------



//--------------------------------------------------------------

//--------------------------------------------------------------



