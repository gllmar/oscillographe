//
//  group_sync.cpp
//  oscillographe
//
//  Created by Guillaume Arseneault on 2018-06-23.
//

#include "group_sync.hpp"


void Group_sync::setup(ofParameterGroup &group, const std::string &host, int remotePort)
{
    syncGroup = group;
    sender.setup(host, 9000);
}

void Group_sync::parse_osc(ofxOscMessage m)
{
    if(m.getAddress()=="/get_gui")
    {
    for (auto& it : syncGroup) {
//        if (*it.==m.getArgAsInt(0))
//        {
//            cout<<"touutut"<<endl;
//        }
        sender.sendParameter(*it);
        //cout << *it << endl;
    }
    }
}
