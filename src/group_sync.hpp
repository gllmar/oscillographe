//
//  group_sync.hpp
//  oscillographe
//
//  Created by Guillaume Arseneault on 2018-06-23.
//

#ifndef group_sync_hpp
#define group_sync_hpp

#include "ofMain.h"
#include "ofxGui.h"
#include "ofxOsc.h"

// devrait avoir un pointeur sur le gui?
// recevoir un message
// passer un groupe vers un port osc
// avoir une fonction de parse osc nesté dans une de get param?


class Group_sync
{
public:
    void setup(ofParameterGroup &group, const std::string &host, int remotePort);
    void parse_osc(ofxOscMessage m);
    
    
private:
    
    /// parameter change callaback
    
    ofxOscSender sender; ///< sync sender
    ofParameterGroup syncGroup; ///< target parameter group
};

#endif /* group_sync_hpp */

