#!/bin/bash
SCRIPTPATH="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

export DISPLAY=:0

cd "$SCRIPTPATH"
../bin/oscillographe&
sleep 3
sendosc 127.0.0.1 9001 /oscillographe/render/fullscreen b false
sleep 1
sendosc 127.0.0.1 9001 /oscillographe/render/fullscreen b true
